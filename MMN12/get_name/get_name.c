/**
 * The program gets input of 3o unique names from the user
 * Each name is max 20 chars long
 * prints the names, and then prints 10 random names.
 * The program assumes the input is valid.
*/

#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>         
#include <time.h>

#define NAME_LENGTH 21 /* 20chars + 0 terminator*/
#define NAME_ARRAY_LENGTH 30

#define RANDOM_NAMES_TO_PRINT 10

#define INPUT_SUCCESS 0
#define INPUT_DUPLICATE_ERROR 1
#define INPUT_MALLOC_ERROR 2

#define DUPLICATE_FOUND 1
#define DUPLICATE_NOT_FOUND 0

char** name_array = NULL;

char* get_name();
int input_names();
void print_names();
void free_names();

int main()
{
    int i=0;
    int ret=0;
    /* init random seed */
    srand(time(NULL));

    /* get names from user */
    ret = input_names();

    if(ret == INPUT_DUPLICATE_ERROR)
    {
        printf("Error: duplicate name was entered!\n");
        free_names();
        return 0;
    }
    else if(ret == INPUT_MALLOC_ERROR)
    {
        printf("Error: program couldn't allocate memory\n");
        free_names();
        return 0;
    }

    /* print input names */
    print_names();

    /* print random names */
    printf("\n\n======== The random names are ========\n");
    for(i=0;i<RANDOM_NAMES_TO_PRINT;i++)
    {
        printf("%02d. %s\n", i+1, get_name());
    }

    /* free memory */
    free_names();
    return 0;
}

/* 
* the function returns ref to a random name
* from the global name array
* @return string: random name
*/
char* get_name()
{
    int randIndex = rand()%NAME_ARRAY_LENGTH;
    return name_array[randIndex];
}

/* 
* the function is an implementation of strcasecmp
* i.e. compares strings without case sensitivity 
* @param char* string1, string to compare to
* @param char* string1, string to for comparison
* @return int:  Less than 0 - string1 less than string2
*               Equals 0 - string1 equals string2
*               Greater than 0 - string1 greater than string2
*/
int strcasecmp(char* string1, char* string2)
{
    char *p1 = string1;
    char *p2 = string2;
    int result;

    do
    {
        result = tolower(*p1) - tolower(*p2++);
        if (*p1++ == '\0')
            break;
    }while(result == 0);
    return result;
}

/* 
* the function checks if given string is a duplicate in the array
* @param char** array, array to check duplicates in
* @param int size, size of array
* @param char* string, check if string is duplicate
* @return int:  DUPLICATE_FOUND if duplicate,
*               DUPLICATE_NOT_FOUND if duplicate not found
*/
int check_duplicate(char** array, int size, char* string)
{
    int i;
    for(i=0;i<size;i++)
    {
        if(!strcasecmp(array[i], string))
            return DUPLICATE_FOUND;
    }
    return DUPLICATE_NOT_FOUND;
}

/* 
* the function handles the input
* of 30 names from the user
* @return int:  INPUT_SUCCESS on success,
*               INPUT_DUPLICATE_ERROR when duplicate was found
*               INPUT_MALLOC_ERROR when allocation error
*/
int input_names()
{
    int i;
    char* name;

    name_array = malloc(NAME_ARRAY_LENGTH * sizeof(char*));
    
    /*check for malloc errors*/
    if(name_array == NULL)
        return INPUT_MALLOC_ERROR;

    printf("Start entering %d names\n", NAME_ARRAY_LENGTH);
    for(i=0;i<NAME_ARRAY_LENGTH;i++)
    {
        name = malloc(NAME_LENGTH * sizeof(char));
        
        /*check for malloc errors*/
        if(name == NULL)
            return INPUT_MALLOC_ERROR;

        /* get input */
        printf("Enter a name: ");
        scanf("%s", name);

        /*check for duplicate before inserting to array*/
        if(check_duplicate(name_array, i, name) == DUPLICATE_FOUND)
            return INPUT_DUPLICATE_ERROR;
        name_array[i] = name;
    }
    return INPUT_SUCCESS;
}

/* 
* the function prints all names in name array
* function assumes the array is fully allocated
* @return void
*/
void print_names()
{
    int i;

    printf("The given names are:\n");
    for(i=0;i<NAME_ARRAY_LENGTH;i++)
    {
        printf("%01d. %s\n", i+1, name_array[i]);
    }
}

/* 
* the function frees the memoray of the array
* @return void
*/
void free_names()
{
    int i;
    for(i=0;i<NAME_ARRAY_LENGTH;i++)
        free(name_array[i]);
    free(name_array);
}
