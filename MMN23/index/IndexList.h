/* 
* contains the data structre of the index list
* and the relevent functions
*/

typedef struct IndexList* IndexList;

/* 
* Creates a new IndexList element
* @param word, word to init element with
* @param row, row to init element with
* @return IndexList, new IndexList element
*/
IndexList newIndexList(char* word, int row);

/* 
* adds a new word and row to the given IndexList
* if word already exist only adds row
* if both already exists does nothing
* @param head, IndexList, head of the list to work on 
* @param word, word to init element with
* @param row, row to init element with
*/
void addWord(IndexList head, char* word, int row);

/* 
* Prints all the IndexList elements
* @param head, IndexList element to print
*/
void printIndexList(IndexList head);