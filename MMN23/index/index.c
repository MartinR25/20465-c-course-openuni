/**
 * The program creates an index for a text file and prints it out.
 * Usage: index file_name
 * the program assumes all the words in the text are seprated by spaces (and EOLs)
 * the program filters out all special characters from the end of a word in the text
 * for example: hello and hello, are the same word, "," will be filtered out
 * the special characters are: ". , : ; ? !"
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "IndexList.h"

#define TRUE 1
#define FALSE 0

/* 
* the function checks if the number of arguments is correct and returns 0
* if not prints a message accordingy and returns 1
* @param argc, number of arguments 
* @return int, 0 if no. of arguments is correct, 1 if incorrect
*/
int check_arguments(int argc)
{
    if(argc > 2)
    {
        printf("Too many arugments given!\nUsage: index file_name\n");
        return 1;
    }
    if(argc == 1)
    {
        printf("No file was given!\nUsage: index file_name\n");
        return 1;
    }
    return 0;
}

/* 
* the function is a wrapper for fopen that handles errors
* @param name, name of the file
* @param mode, mode to open the file with 
* @return FILE, pointer to file
*/
FILE *my_open (char* name, char* mode)
{
    FILE *fp;
    char msg[30];
    fp = fopen (name, mode);
    if (fp == NULL)
    {
        sprintf(msg, "%s", name);
        perror(msg);
        exit (1);
    }
    return (fp);
} 

/* 
* the function gets a string and filters out
* ". , : ; ? !" from the end of the word
* and returns a new word without them
* @param word, a string to filter out
* @return string, the word filtered
*/
char* filterWord(char* word)
{
    int i;
    int length;
    char* filteredWord;
    int reachedWord = FALSE;
    length = strlen(word);
    
    i = length-1;
    while(i>=0 && !reachedWord)
    {
        if(word[i] == ',' || word[i] == ':' || word[i] == ';' ||
         word[i] == '?' || word[i] == '!' || word[i] == '.')
            length--; 
        else
            reachedWord = TRUE;
        i--;
    }
    filteredWord = malloc((length+1)*sizeof(char));
    strncpy(filteredWord,word,length);
    filteredWord[length] = '\0';
    return filteredWord;
}

int main(int argc, char** argv)
{
    char buffer[1000];
    char* filteredBuffer;
    char temp;
    int scanCount;
    int rowCount = 1;
    FILE* fp;
    IndexList list = NULL;

    if(check_arguments(argc))
        return 1;

    fp = my_open (argv[1], "r"); 

    /* read word and a character to determine if its a eol or space*/
    scanCount  = fscanf(fp, " %1000s%c", buffer, &temp);
    if(scanCount == 0)
        return 0;

    if(temp == '\n')
        rowCount++;

    filteredBuffer = filterWord(buffer);
    list = newIndexList(filteredBuffer, 1);
    free(filteredBuffer);
    while (!feof(fp))
    {
        /* read word and a character to determine if its a eol or space*/
        fscanf(fp, " %1000s%c", buffer, &temp);
        filteredBuffer = filterWord(buffer);
        addWord(list, filteredBuffer, rowCount);
        free(filteredBuffer);
        if(temp == '\n')
            rowCount++;
    }

    printIndexList(list);
    fclose(fp);
    return 0;
}