#include "IndexList.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TRUE 1
#define FALSE 0

/* 
* IndexList structure,
* works like linked list,
* but contains additonal fields
* to hold data relevent to the index
*/
struct IndexList
{
    char* word;
    int* rows;
    int numberOfRows;
    struct IndexList* next;
};

/* 
* this is a helper function.
* checks if row exists in the IndexList element
* @param element, IndexList element to check on
* @param row, row to check if exists
* @returns int, TRUE(1) if exists, FALSE(0) if not
*/
int isNewRow(IndexList element, int row)
{
    int i;
    for(i=0;i<element->numberOfRows;i++)
    {
        if(element->rows[i] == row)
            return FALSE;
    }
    return TRUE;
}

/* 
* this is a helper function.
* adds a row to the rows list in the IndexList element
* if it doesnt exists already
* @param element, IndexList element to work on
* @param row, row to add
*/
void addRow(IndexList element, int row)
{
    int i;
    int* tempArray;

    if(element == NULL)
        return;

    if(element->numberOfRows == 0)
    {
        element->numberOfRows = 1;
        element->rows = malloc(element->numberOfRows*sizeof(int));
        element->rows[0] = row;
    }
    else
    {
        if(isNewRow(element, row))
        {
            /* create a new array and copy all the previous data */
            element->numberOfRows += 1;
            tempArray = malloc(element->numberOfRows*sizeof(int));
            for(i=0;i<element->numberOfRows-1;i++)
                tempArray[i] = element->rows[i];
            tempArray[element->numberOfRows-1] = row;
            free(element->rows);
            element->rows = tempArray;
        }
    }
}

IndexList newIndexList(char* word, int row)
{
    char* newWord;
    IndexList list = malloc(sizeof(struct IndexList));
    
    /* deep copy word */
    newWord = malloc(sizeof(char) * (strlen(word)+1));
    strcpy(newWord, word);

    list->word = newWord;
    list->numberOfRows = 0;
    list->next = NULL;
    addRow(list, row);
    
    return list;
}

void addWord(IndexList head, char* word, int row)
{
    char* newWord;
    int isWordFound = FALSE;
    IndexList newElement;
    IndexList currentElement = head;
    IndexList lastElement = head;

    /* if list is null we cant add a word*/
    if(head == NULL)
        return;

    do
    {
        if(strcmp(word, currentElement->word) < 0)
        {
            /* deep copy index */
            newElement = newIndexList(currentElement->word, 1);
            newElement->numberOfRows = currentElement->numberOfRows;
            newElement->rows = currentElement->rows;
            newElement->next = currentElement->next;

            /* deep copy word */
            newWord = malloc(sizeof(char) * (strlen(word)+1));
            strcpy(newWord, word);

            /* replace current element with new word*/
            currentElement->word = newWord;
            currentElement->numberOfRows = 1;
            currentElement->rows = malloc(currentElement->numberOfRows*sizeof(int));
            currentElement->rows[0] = row;
            currentElement->next = newElement;
            isWordFound = TRUE;
        }
        else if(strcmp(currentElement->word, word) == 0)
        {
            addRow(currentElement, row);
            isWordFound = TRUE;
        }
        lastElement = currentElement;
        currentElement = currentElement->next;
    } while(currentElement && !isWordFound);

    /* if word not found add it to list*/
    if(!isWordFound)
    {
        lastElement->next = newIndexList(word, row);
    }
}

void printIndexList(IndexList head)
{
    int i;
    IndexList temp = head;
    while(temp)
    {
        if(temp->numberOfRows > 1)
        {
            printf("%s\tappears in lines %d", temp->word, temp->rows[0]);
            for(i=1;i<temp->numberOfRows;i++)
                printf(",%d", temp->rows[i]);
            printf("\n");
        }
        else
            printf("%s\tappears in line %d\n", temp->word, temp->rows[0]);
        temp = temp->next;
    }
}