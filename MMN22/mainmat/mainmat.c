/**
 * The program is a "calculator" for matrices.
 * The program gets input of commands and arguments from the user.
 * The program hold 6 matrices, which the user can maniuplate with commands.
 * The program assumes there is a 1000char limit per command, 
 * as with correct input it should not be possible to get over 600chars
 * (float can have a max 34digits, maxium of 16floats + commands < 600chars)  
 * The program checks for errors and problems with the commands and warns the user about it.
*/
#include "mymat.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER_SIZE 1000 /*max buffer size*/
#define MAX_NUMBER_OF_ARGUMENTS 17 /* max arguments*/

#define TRUE 1
#define FALSE 0

/* Status of input parsing */
typedef enum InputStatus{
    SUCCESS,
    UNDEFINED_MATRIX,
    UNDEFINED_COMMAND,
    NOT_REAL_NUMBER,
    EXTRANEOUS_TEXT,
    MISSING_ARGUMENT,
    ILLEGAL_COMMA,
    MISSING_COMMA,
    CONSECUTIVE_COMMAS,
    NOT_SCALAR,
    EOF_REACHED
} InputStatus;

/* command names (values) */
#define READ_COMMAND   "read_mat"
#define PRINT_COMMAND  "print_mat"
#define ADD_COMMAND    "add_mat"
#define SUB_COMMAND    "sub_mat"
#define MUL_COMMAND    "mul_mat"
#define SCALAR_COMMAND "mul_scalar"
#define TRANS_COMMAND  "trans_mat"
#define STOP_COMMAND   "stop"

/* matrices names (values) */
#define MAT_A_NAME "MAT_A"
#define MAT_B_NAME "MAT_B"
#define MAT_C_NAME "MAT_C"
#define MAT_D_NAME "MAT_D"
#define MAT_E_NAME "MAT_E"
#define MAT_F_NAME "MAT_F"

/* matrices indexes*/
typedef enum MatrixIndex {
    UNKNOWN_INDEX = -1,
    MAT_A_INDEX = 0,
    MAT_B_INDEX,
    MAT_C_INDEX,
    MAT_D_INDEX,
    MAT_E_INDEX,
    MAT_F_INDEX
} MatrixIndex;

/* 
* the function parses the command name from the command input
* @param char* input, command and arguments entered by the user
* @param char** command, address to store the command in
* @return InputStatus:  status of the command parsing
*/
InputStatus get_command(char* input, char** command);

/* 
* the function parses the arguments from the command input
* @param char* input, command and arguments entered by the user
* @param char*** args, address to store the argument array (string array)
* @return InputStatus:  status of the argument parsing
*/
InputStatus get_args(char* input, char*** args);

/* 
* the function checks if the arguments for the stop command are valid
* @param char** args, arguments entered by the user
* @return InputStatus:  status of the argument validating
*/
InputStatus is_valid_stop_arguments(char** args);

/* 
* the function checks if the arguments for the read command are valid
* and parses them if it is.
* @param char** args, arguments entered by the user
* @param int* matIndex, address to store the matrix index from the arguments
* @param float[16] output, values array to store the float arguments in
* @return InputStatus:  status of the argument validating
*/
InputStatus is_valid_read_args(char** args, int* matIndex, double output[16]);

/* 
* the function checks if the arguments for the print command are valid
* and parses them if it is.
* @param char** args, arguments entered by the user
* @param int* matIndex, address to store the matrix index from the arguments
* @return InputStatus:  status of the argument validating
*/
InputStatus is_valid_print_args(char** args, int* matIndex);

/* 
* the function checks if the arguments for the add,sub and mul commands are valid
* and parses them if it is.
* @param char** args, arguments entered by the user
* @param int* matIndex1, address to store the first matrix index from the arguments
* @param int* matIndex2, address to store the second matrix index from the arguments
* @param int* matIndex3, address to store the third matrix index from the arguments
* @return InputStatus:  status of the argument validating
*/
InputStatus is_valid_arithmetic_args(char** args, int* matIndex1, int* matIndex2, int* matIndex3);

/* 
* the function checks if the arguments for the mul_scalar command are valid
* and parses them if it is.
* @param char** args, arguments entered by the user
* @param int* matIndex1, address to store the first matrix index from the arguments
* @param float* scalar, address to store the scalar value in from the arguments
* @param int* matIndex3, address to store the third matrix index from the arguments
* @return InputStatus:  status of the argument validating
*/
InputStatus is_valid_scalar_args(char** args, int* matIndex1, float* scalar, int* matIndex2);

/* 
* the function checks if the arguments for the transform command are valid
* and parses them if it is.
* @param char** args, arguments entered by the user
* @param int* matIndex1, address to store the first matrix index from the arguments
* @param int* matIndex2, address to store the third matrix index from the arguments
* @return InputStatus:  status of the argument validating
*/
InputStatus is_valid_trans_args(char** args, int* matIndex1, int* matIndex2);

/* 
* helper function for the validators functions. 
* function returns the matrix index according to the matrix name.
* @param char* name, name of the matrix
* @return MatrixIndex: index of the matrix if it exists
*/
MatrixIndex get_mat_index(char* name);

/* 
* function prints the error msg for each error
* @param InputStatus, inputStatus (which is one of the errors)
*/
void print_error(InputStatus status);

int main()
{
    /*mat MAT_A, MAT_B, MAT_C, MAT_D, MAT_E, MAT_F;*/
    mat MAT_ARR[6];

     /* index for mats used in commands */
    int index1;
    int index2;
    int index3;
    
    char* command;
    char** args = NULL;
    double readValuesBuffer[16];
    char* buffer = malloc(sizeof(char)*BUFFER_SIZE);
    int stop = FALSE;
    int status;
    float scalar;

    /* init MAT_ARR */
    reset_mat(MAT_ARR[MAT_A_INDEX]);
    reset_mat(MAT_ARR[MAT_B_INDEX]);
    reset_mat(MAT_ARR[MAT_C_INDEX]);
    reset_mat(MAT_ARR[MAT_D_INDEX]);
    reset_mat(MAT_ARR[MAT_E_INDEX]);
    reset_mat(MAT_ARR[MAT_F_INDEX]);
    
    while(!stop)
    {
        /* get user input */ 
        printf("Please enter a command:\n");
        
        if(fgets (buffer, BUFFER_SIZE, stdin) != NULL)
        {
            status = get_command(buffer, &command);
            if(status == SUCCESS)
                /* offset input by the command length */
                status = get_args(buffer+strlen(command), &args);
        }
        else
        {
            status = EOF_REACHED;
            stop = TRUE;
        }

        if(status == SUCCESS)
        {
            /* check which command to execute*/
            if(strcmp(command, READ_COMMAND) == 0) 
            {
                status = is_valid_read_args(args, &index1, readValuesBuffer);
                if(status == SUCCESS)
                {
                    read_mat(MAT_ARR[index1], readValuesBuffer);
                }
            } 
            else if(strcmp(command, PRINT_COMMAND) == 0)
            {
                status = is_valid_print_args(args, &index1);
                if(status == SUCCESS)
                    print_mat(MAT_ARR[index1]);
            }
            else if(strcmp(command, ADD_COMMAND) == 0)
            {
                status = is_valid_arithmetic_args(args, &index1, &index2, &index3);
                if(status == SUCCESS)
                    add_mat(MAT_ARR[index1], MAT_ARR[index2], MAT_ARR[index3]);
            }
            else if(strcmp(command, SUB_COMMAND) == 0)
            {
                status = is_valid_arithmetic_args(args, &index1, &index2, &index3);
                if(status == SUCCESS)
                    sub_mat(MAT_ARR[index1], MAT_ARR[index2], MAT_ARR[index3]);
            }
            else if(strcmp(command, MUL_COMMAND) == 0)
            {
                status = is_valid_arithmetic_args(args, &index1, &index2, &index3);
                if(status == SUCCESS)
                    mul_mat(MAT_ARR[index1], MAT_ARR[index2], MAT_ARR[index3]);
            }
            else if(strcmp(command, SCALAR_COMMAND) == 0)
            {
                status = is_valid_scalar_args(args, &index1, &scalar, &index2);
                if(status == SUCCESS)
                    mul_scalar(MAT_ARR[index1], scalar, MAT_ARR[index2]);
            }
            else if(strcmp(command, TRANS_COMMAND) == 0)
            {
                status = is_valid_trans_args(args, &index1, &index2);
                if(status == SUCCESS)
                    trans_mat(MAT_ARR[index1], MAT_ARR[index2]);
            }
            else if(strcmp(command, STOP_COMMAND) == 0)
            {
                status = is_valid_stop_arguments(args);
                if(status == SUCCESS)
                {
                    printf("Stop was entered, stopping execution.\n");
                    stop = TRUE;
                }
            }
            else
            {
                status = UNDEFINED_COMMAND;
            }
        }

        if(status != SUCCESS)
            print_error(status);

        free(command);
        free(args);
        command = NULL;
        args = NULL;
    }
    return 0;
}


InputStatus get_command(char* input, char** command)
{
    char* firstWhitespace;
    char* firstTab;
    char* firstEOL;
    char* firstEnter;
    char* firstComma;
    char* commandEnd;

    firstWhitespace = strchr(input, ' ');
    firstEnter = strchr(input, '\n');
    firstComma = strchr(input, ',');
    firstTab = strchr(input, '\t');
    firstEOL = strchr(input, '\0');

    /* determine which comes first */
    if(firstWhitespace == NULL && firstTab == NULL)
        commandEnd= firstEnter == NULL ? firstEOL : firstEnter;
    else if(firstWhitespace == NULL)
        commandEnd = firstTab;
    else if(firstTab == NULL)
        commandEnd = firstWhitespace;
    else
        commandEnd = firstWhitespace < firstTab ? firstWhitespace : firstTab;    

    /*if the comma is before any one the above its illegal*/
    if(firstComma != NULL && firstComma < commandEnd)
        return ILLEGAL_COMMA;

    *command =  malloc(sizeof(char)*(commandEnd-input+1));
    strncpy(*command, input, commandEnd-input);
    (*command)[commandEnd-input] = '\0';
    return SUCCESS;
}

InputStatus get_args(char* input, char*** args)
{
    int i, offset;
    int reachedEOL = FALSE;
    char* endOfArugment;
    char* temp;

    *args = malloc(sizeof(char*)*MAX_NUMBER_OF_ARGUMENTS);
    if(strchr(input, '\n') == input || strchr(input, '\0') == input)
        reachedEOL = TRUE;

    for(i=0;i<MAX_NUMBER_OF_ARGUMENTS;i++)
    {
        if(!reachedEOL)
        {
            endOfArugment = strchr(input, ',');
            if(endOfArugment == NULL)
            {
                endOfArugment = strchr(input, '\0');
                reachedEOL = TRUE;
            }
            /* remove pre and post padding of whitespaces and tabs */
            while((*input == ' ' || *input == '\t') && input<=endOfArugment)
            {
                input++;
            }
            offset = endOfArugment-input+1;
            while((*endOfArugment == ' ' || *endOfArugment == '\t' || *endOfArugment == '\0' || *endOfArugment == ',' || *endOfArugment == '\n') && input<=endOfArugment)
                endOfArugment--;
            /* if args >  endOfArugment it means there is nothing but whitepsace between 2 commas */
            if(input>endOfArugment)
                return !reachedEOL ? CONSECUTIVE_COMMAS : EXTRANEOUS_TEXT;  
            /* copy arg to temp */
            temp = malloc(sizeof(char)*(endOfArugment-input+2));
            strncpy(temp, input, endOfArugment-input+1);
            temp[endOfArugment-input+1] = '\0';
            /* check if argument contains whitespace */
            if(strchr(temp, ' ') != NULL || strchr(temp, '\t') != NULL)
                return MISSING_COMMA;
            (*args)[i] = temp;
            input += offset;
        }
        else
        {
            (*args)[i] = "\0";
        }
    }
    return SUCCESS;
}

InputStatus is_valid_number(char* number)
{
    while(*number!='\0')
    {
        if((*number < '0' || *number > '9') && *number != '-' && *number != '.')
            return NOT_REAL_NUMBER;
        number++;
    }
    return SUCCESS;
}

InputStatus is_valid_stop_arguments(char** args)
{
    int i;
    /* check for extra (illegal) arguments */
    for(i=0;i<MAX_NUMBER_OF_ARGUMENTS;i++)
    {
        if(strcmp(args[i], "\0") != 0)
            return EXTRANEOUS_TEXT;
    }
    return SUCCESS;
}

InputStatus is_valid_read_args(char** args, int* matIndex, double output[16])
{
    int i;
    int temp;
    int status;
    
    /* check that all arguments are present */
    if(strcmp(args[0], "\0") == 0 || strcmp(args[1], "\0") == 0)
        return MISSING_ARGUMENT;

    /* get matrices indexes */
    temp = get_mat_index(args[0]);
    if(temp == UNKNOWN_INDEX)
        return UNDEFINED_MATRIX;
    *matIndex = temp; 

    for(i=1;i<MAX_NUMBER_OF_ARGUMENTS;i++)
    {
        if(strcmp(args[i], "\0") != 0)
        {

            status = is_valid_number(args[i]);
            if(status != SUCCESS)
                return status;        
            output[i-1] = atof(args[i]);
        }
        else
        {
            output[i-1] = 0;
        }
    }
    return SUCCESS;
}

InputStatus is_valid_print_args(char** args, int* matIndex)
{
    int i;
    int temp;

    /* check that all arguments are present */
    if(strcmp(args[0], "\0") == 0)
        return MISSING_ARGUMENT;

    /* get matrices indexes */
    temp = get_mat_index(args[0]);
    if(temp == UNKNOWN_INDEX)
        return UNDEFINED_MATRIX;
    *matIndex = temp;

    /* check for extra (illegal) arguments */
    for(i=1;i<MAX_NUMBER_OF_ARGUMENTS;i++)
    {
        if(strcmp(args[i], "\0") != 0)
            return EXTRANEOUS_TEXT;
    }

    return SUCCESS;
}

InputStatus is_valid_arithmetic_args(char** args, int* matIndex1, int* matIndex2, int* matIndex3)
{
    int i;
    int temp1, temp2, temp3;

    /* check that all arguments are present */
    if(strcmp(args[0], "\0") == 0 || strcmp(args[1], "\0") == 0 || strcmp(args[2], "\0") == 0)
            return MISSING_ARGUMENT;

    /* get matrices indexes */
    temp1 = get_mat_index(args[0]);
    temp2 = get_mat_index(args[1]);
    temp3 = get_mat_index(args[2]);
    if(temp1 == UNKNOWN_INDEX || temp2 == UNKNOWN_INDEX || temp3 == UNKNOWN_INDEX)
        return UNDEFINED_MATRIX;
    *matIndex1 = temp1;
    *matIndex2 = temp2;
    *matIndex3 = temp3;

    /* check for extra (illegal) arguments */
    for(i=3;i<MAX_NUMBER_OF_ARGUMENTS;i++)
    {
        if(strcmp(args[i], "\0") != 0)
            return EXTRANEOUS_TEXT;
    }

    return SUCCESS;
}

InputStatus is_valid_scalar_args(char** args, int* matIndex1, float* scalar, int* matIndex2)
{
    int i, status;
    int temp1, temp2;

    /* check that all arguments are present */
    if(strcmp(args[0], "\0") == 0 || strcmp(args[1], "\0") == 0 || strcmp(args[2], "\0") == 0)
        return MISSING_ARGUMENT;

    /* get matrices indexes */
    temp1 = get_mat_index(args[0]);
    temp2 = get_mat_index(args[2]);
    if(temp1 == UNKNOWN_INDEX || temp2 == UNKNOWN_INDEX)
        return UNDEFINED_MATRIX;
    *matIndex1 = temp1;
    *matIndex2 = temp2;

    status = is_valid_number(args[1]);
    if(status != SUCCESS)
        return NOT_SCALAR;
    *scalar = atof(args[1]);
    
    /* check for extra (illegal) arguments */
    for(i=3;i<MAX_NUMBER_OF_ARGUMENTS;i++)
    {
        if(strcmp(args[i], "\0") != 0)
            return EXTRANEOUS_TEXT;
    }
    return SUCCESS;
}

InputStatus is_valid_trans_args(char** args, int* matIndex1, int* matIndex2)
{
    int i;
    int temp1, temp2;

    /* check that all arguments are present */
    if(strcmp(args[0], "\0") == 0 || strcmp(args[1], "\0") == 0)
        return MISSING_ARGUMENT;

    /* get matrices indexes */
    temp1 = get_mat_index(args[0]);
    temp2 = get_mat_index(args[1]);
    if(temp1 == UNKNOWN_INDEX || temp2 == UNKNOWN_INDEX)
        return UNDEFINED_MATRIX;
    *matIndex1 = temp1;
    *matIndex2 = temp2;

    /* check for extra (illegal) arguments */
    for(i=2;i<MAX_NUMBER_OF_ARGUMENTS;i++)
    {
        if(strcmp(args[i], "\0") != 0)
            return EXTRANEOUS_TEXT;
    }

    return SUCCESS;
}

void print_error(InputStatus status)
{
    switch(status)
    {
        case UNDEFINED_MATRIX: 
            printf("Undefined matrix name\n");
            break;
        case UNDEFINED_COMMAND:
            printf("Undefined command name\n");
            break;
        case NOT_REAL_NUMBER: 
            printf("Argument is not a real number\n");
            break;
        case EXTRANEOUS_TEXT: 
            printf("Extraneous text after end of command\n");
            break;
        case MISSING_ARGUMENT: 
            printf("Missing argument\n");
            break;
        case ILLEGAL_COMMA: 
            printf("Illegal comma\n");
            break;
        case MISSING_COMMA: 
            printf("Missing comma\n");
            break;
        case CONSECUTIVE_COMMAS: 
            printf("Multiple consecutive commas\n");
            break;
        case NOT_SCALAR: 
            printf("Argument is not a scalar\n");
            break;
        case EOF_REACHED:
            printf("EOF was entered, its not a valid stop! but program will shutdown.");
            break;
        default:
            printf("Unknown Error!\n");
    }
}

MatrixIndex get_mat_index(char* name)
{
    if(strcmp(name, MAT_A_NAME) == 0)
        return MAT_A_INDEX;
    if(strcmp(name, MAT_B_NAME) == 0)
        return MAT_B_INDEX;
    if(strcmp(name, MAT_C_NAME) == 0)
        return MAT_C_INDEX;
    if(strcmp(name, MAT_D_NAME) == 0)
        return MAT_D_INDEX;
    if(strcmp(name, MAT_E_NAME) == 0)
        return MAT_E_INDEX;
    if(strcmp(name, MAT_F_NAME) == 0)
        return MAT_F_INDEX;
    return UNKNOWN_INDEX;
}