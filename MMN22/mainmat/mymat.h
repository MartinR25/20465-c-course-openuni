/*
    Matrix type and functions file
*/

/* Defines the matrix type which is double[4][4]*/
typedef double mat[4][4];

/* 
* the function resets all values of the mat to 0
* @param mat matrix, matrix to reset
*/
void reset_mat(mat matrix);

/* 
* prints the matrix values on a 4x4 grid
* @param mat matrix, matrix to print
*/
void print_mat(mat matrix);

/* 
* the function sets all values of the matrix
* according to the input array
* @param mat matrix, matrix to set values in
* @param float[16] input, values to set in matrix
*/
void read_mat(mat matrix, double input[16]);

/* 
* the function implements the addition operation of Matrices 
* @param mat augend, matrix to operate with
* @param mat addend, matrix to operate with
* @param mat sum, matrix to put the values in: sum = augend + addend
*/
void add_mat(mat augend, mat addend, mat sum);

/* 
* the function implements the subtraction operation of Matrices 
* @param mat minuned, matrix to operate with
* @param mat subtrahend, matrix to operate with
* @param mat difference, matrix to put the values in: difference = minuned - subtrahend
*/
void sub_mat(mat minuned, mat subtrahend, mat difference);

/* 
* the function implements the multiplication operation of Matrices 
* @param mat multipler, matrix to operate with
* @param mat multiplicand, matrix to operate with
* @param mat product, matrix to put the values in: product = multipler * multiplicand
*/
void mul_mat(mat multipler, mat multiplicand, mat product);

/* 
* the function implements the multiplication by a scalar operation of Matrices 
* @param mat matrix, matrix to operate with
* @param float scalar, scalar value to operate with
* @param mat product, matrix to put the values in: product = matrix * scalar
*/
void mul_scalar(mat matrix, double scalar, mat product);

/* 
* the function implements the transform operation of Matrices 
* @param mat matrix, matrix to operate with
* @param mat transpose, matrix to put transformation matrix in
*/
void trans_mat(mat matrix, mat transpose);
