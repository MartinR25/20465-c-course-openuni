#include "mymat.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define WIDTH 4
#define LENGTH 4

#define FALSE 0
#define TRUE 1

void reset_mat(mat matrix)
{
    int x,y;
    
    for(x=0;x<LENGTH;x++)
    {
        for(y=0;y<WIDTH;y++)
        {
            matrix[x][y] = 0;
        }
    }
}

/* 
* the function deep copies a matrix,
* i.e. copies the values to a different matrix
* @param mat src, matrix to copy values from
* @param mat dest, matrix to copy values to
*/
void copy_mat(mat src, mat dest)
{
    int x,y;

    for(x=0;x<LENGTH;x++)
    {
        for(y=0;y<WIDTH;y++)
        {
            dest[x][y] = src[x][y];
        }
    }
}

void print_mat(mat matrix)
{
    int x,y;
    
    for(x=0;x<LENGTH;x++)
    {
        for(y=0;y<WIDTH;y++)
        {
            if(y==0)
                printf("%.5f", matrix[x][y]);
            else
                printf(",\t%.5f", matrix[x][y]);
        }
        printf("\n");
    }
}


void read_mat(mat matrix, double input[16])
{
    int x,y;

    for(x=0;x<LENGTH;x++)
    {
        for(y=0;y<WIDTH;y++)
        {
            matrix[x][y] = input[x*4+y];
        }
    }
}

void add_mat(mat augend, mat addend, mat sum)
{
    int x,y;

    for(x=0;x<LENGTH;x++)
    {
        for(y=0;y<WIDTH;y++)
        {
            sum[x][y] = augend[x][y] + addend[x][y];
        }
    }
}

void sub_mat(mat minuned, mat subtrahend, mat difference)
{
    int x,y;

    for(x=0;x<LENGTH;x++)
    {
        for(y=0;y<WIDTH;y++)
        {
            difference[x][y] = minuned[x][y] - subtrahend[x][y];
        }
    }
}

/* 
* helper function for mul_mat,
* the function calculates the value from multiplying 
* a row from the multiplier matrix
* by a column from the multiplicand matrix
* @param mat multiplier, matrix to take a row from
* @param mat multiplicand, matrix to take a column from
*/
float multiply_row_by_col(mat multiplier, mat multiplicand, int row, int col)
{
    int index = 0;
    float sum = 0;
    
    for(index = 0;index<LENGTH;index++)
    {
        sum += multiplier[row][index] * multiplicand[index][col];
    }
    return sum;
}

void mul_mat(mat multiplier, mat multiplicand, mat product)
{
    int x,y;
    mat temp;

    for(x=0;x<LENGTH;x++)
    {
        for(y=0;y<WIDTH;y++)
        {
            temp[x][y] = multiply_row_by_col(multiplier, multiplicand, x, y);
        }
    }
    copy_mat(temp, product);
}


void mul_scalar(mat matrix, double scalar, mat product)
{
    int x,y;
    
    for(x=0;x<LENGTH;x++)
    {
        for(y=0;y<WIDTH;y++)
        {
            product[x][y] = matrix[x][y] * scalar;
        }
    }
}

void trans_mat(mat matrix, mat transpose)
{
    int x,y;
    mat temp;

    for(x=0;x<LENGTH;x++)
    {
        for(y=0;y<WIDTH;y++)
        {
            transpose[x][y] = matrix[y][x];
        }
    }
    copy_mat(temp, transpose);
}