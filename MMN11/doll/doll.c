/**
 * The program gets input of N float values
 * First value is used as the convertion rate from USD to NIS
 * Second value to N value are values to be converted and will be showed in the table
 * After the input the program prints the convertion table with the given values and the last line is the sum of those values.
 * The program assumes the input is valid.
*/

#include <stdio.h>         

#define TRUE 1
#define FALSE 0

int main()
{
    char enterKey; /* will be used to determine if enter was pressed*/
    double input, converstionRate, sum = 0; /*input will be used as a buffer, conversionRate will hold the first value given, sum will be used to sum all input */
    int isFirstNumber = TRUE; /* used as boolean to determine if the input is the first number or not */

    printf("Enter all numbers in one line, and press enter to stop:\n");
    /* keep scanning untill EOF is hit*/
    while(scanf("%lf", &input) > 0)
    {
        /* if input is first number print header and assign converstionRate value, else print table row*/
        if(isFirstNumber)
        {
            printf("$\t\t\t\tIS\n");
            converstionRate = input;
            isFirstNumber = FALSE;
        }
        else
        {
            printf("%.2f\t\t\t\t%.2f\n", input, input*converstionRate);
            sum += input;
        }
        
        /* stop input when enter is pressed */
        scanf("%c", &enterKey);
        if(enterKey == '\n')
            break;
    }
    printf("%.2f\t\t\t\t%.2f\n", sum, sum*converstionRate);
    return 0;
}