/**
 * The program gets input an angle (in radians)
 * And prints 3 values:
 * given angle, my_cos output, and math.h cos output
 * this compares the the implementation of cos vs the std cos
*/
#include <stdio.h>
#include <math.h>

#define MY_COS_TOLERENCE 0.000001
#define PI 3.14159265358979323846

#define TRUE 1
#define FALSE 0

/* 
* the function calculates the factorial value of num
* @param num, int to calculate the factorial 
* @return factorial of num
*/
double factorial(int num)
{
    if(num == 0)
        return 1.0;
    return num * factorial(num-1);
}

/* 
* the function normalizes the angle to be in the bounds of 0 < angle < 2PI
* @param angle, the angle in radians.
* @return angle in the bounds of 0 <= angle <= 2PI
*/
double normalize_angle(double angle)
{
    const double twopi = 2 * PI; /* hold 2PI */
    return angle - twopi * floor(angle/twopi);
}

/* 
* the function calculates the cos of the given angle
* using Taylor series for cosinus.
* @param angle, the angle in radians.
* @return cos of angle
*/
double my_cos(double angle) {
    double output = 0.0; /* value to be returned*/
    double element = 0; /* temp value to hold the calculation of each element in the algorithem*/
    int i=0; /* index */
    do
    {
        element= pow(angle, 2*i) / factorial(2*i);
        output += pow(-1, i) * element;
        i++;
    }while(element > MY_COS_TOLERENCE); 
    return output;
}

int main()
{
    double input; /* used as buffer for input */

    /* get input from user */
    printf("Enter a number to use the cos function on:\n");
    scanf("%lf", &input);

    /* print my_cos and cos functions output */
    printf("input:\t %.6f\n", input);
    printf("my_cos:\t %.6f\n", my_cos(normalize_angle(input)));
    printf("<math.h> cos:\t %.6f\n", cos(input));
    return 0;
}